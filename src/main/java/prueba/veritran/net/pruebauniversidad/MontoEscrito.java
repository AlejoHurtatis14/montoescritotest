package prueba.veritran.net.pruebauniversidad;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MontoEscrito {
    public static Map<Integer, String> parseoNumeros = new HashMap<Integer, String>();
    public static void main(String[] args) {
        //Neider Alejandro Hurtatis - 4 Semestre - Moviles
        Scanner sc = new Scanner(System.in);
        MontoEscrito montoEscrito = new MontoEscrito();
        System.out.println("Cordial saludo querido usuario.\n"
                + "Este es un software para convertir un número a texto.");
        Integer valor = 1;
        do {
            try {
                System.out.println("Por favor ingrese un numero menor a 1'000.000.000 o si quiere salir por favor ingrese un 0(cero)");
                valor = sc.nextInt();
                if (valor >= 1000000000) {
                    System.out.println("Numero Erroneo");
                } else {
                    String resultado = montoEscrito.getMontoEscrito(valor);
                    System.out.println(resultado);
                }
            } catch (Exception e) {
                System.out.println("Solo puede ingresar numeros");
                sc.next();
            }

        } while (valor != 0);
    }

    public static String getMontoEscrito(Integer valor) {
        crearParseNumeros();
        //Validamos si el valor ingresado esta en el Map
        if (parseoNumeros.containsKey(valor)) {
            return parseoNumeros.get(valor);
        }
        //Obtenemos los dos primeros numeros del valor ingresado de la parte de centenas.
        String resultadocien = "";
        int NumeroUnocien = valor % 10;//Tenemos el primer numero del valor
        valor = valor / 10;//Eliminamos el primer numero del valor
        int NumeroDoscien = valor % 10;//Tenemos el segundo numero del valor
        valor = valor / 10;//Eliminamos el segundo numero del valor
        int busquedacien = (NumeroDoscien * 10) + NumeroUnocien;
        if (NumeroUnocien != 0 || NumeroDoscien != 0) { //Validamos si uno de los numeros son diferentes de cero buscamos el texto de esos dos numeros
            resultadocien = decenas(busquedacien, resultadocien, NumeroUnocien, NumeroDoscien, valor, 0);
        }
        int ciento = valor % 10;
        resultadocien = numerosCien(ciento, resultadocien, NumeroUnocien, NumeroDoscien);//Vamos por el numero en centenas
        valor = valor / 10;
        String resultadomil = "";
        if (valor > 0) {//Validamos que el valor todavia hayan numeros para pasa a texto sino retorna el texto de centecimas
            //Lo mismo que lo anterior obtenemos los tres numeros de la parte de miles
            int NumeroUnomil = valor % 10;
            valor = valor / 10;
            int NumeroDosmil = valor % 10;
            valor = valor / 10;
            int busquedamil = (NumeroDosmil * 10) + NumeroUnomil;
            resultadomil = decenas(busquedamil, resultadomil, NumeroUnomil, NumeroDosmil, valor, 1);//Buscamos el numero en decenas
            ciento = valor % 10;
            if (NumeroUnomil == 9) {//validamos que si el primer numero es 9 nos quede nuevemil
                resultadomil = numerosCien(ciento, resultadomil, NumeroUnomil, NumeroDosmil) + "mil ";
            } else if (NumeroUnomil == 1 && valor == 0) {//validamos que si el primer numero es 1 nos quede mil
                resultadomil = parseoNumeros.get(1000) + " ";
            } else { //Si no cumple esas restricciones entonces traducimos el numero normal con el metodo.
                resultadomil = numerosCien(ciento, resultadomil, NumeroUnomil, NumeroDosmil) + " mil ";
            }
            ciento = valor % 10;
            valor = valor / 10;
        } else {
            return resultadocien;
        }
        String resultadomillon = "";
        if (valor > 0) {//Validamos que todavia existan numeros para traducir y obtenemos cada numero en una variable sino retornamos el valor hasta miles
            int NumeroUnomillon = valor % 10;
            valor = valor / 10;
            int NumeroDosmillon = valor % 10;
            valor = valor / 10;
            int busquedamillon = (NumeroDosmillon * 10) + NumeroUnomillon;
            resultadomillon = decenas(busquedamillon, resultadomillon, NumeroUnomillon, NumeroDosmillon, valor, 2);//Cojemos los dos primeros numeros y los buscamos en decenas
            if (NumeroUnomillon == 1 && valor == 0) { //Validamos que el numero sea 1 para que se muestre como millon
                resultadomillon = parseoNumeros.get(1000000) + " ";
            } else { //Sino buscamos el numero de forma normal
                resultadomillon = numerosCien(valor, resultadomillon, NumeroUnomillon, NumeroDosmillon) + " millones ";
            }
        } else {
            return resultadomil + "" + resultadocien;
        }
        return resultadomillon + "" + resultadomil + "" + resultadocien;//Retorno resultado
    }

    //Conctructor para buscar el numero en centecimas
    public static String numerosCien(int valor, String texto, int NumeroUno, int NumeroDos) {
        int busqueda = valor * 100;
        if (valor != 0) {
            if (NumeroUno == 0 && NumeroDos == 0) {
                texto = parseoNumeros.get(valor) + "cientos";
            } else {
                if (valor == 1) {
                    texto = parseoNumeros.get(busqueda) + "to " + texto;
                } else {
                    if (valor == 9) {
                        texto = "novecientos " + texto;
                    } else {
                        if (valor == 7) {
                            texto = "setecientos " + texto;
                        } else {
                            if (parseoNumeros.containsKey(busqueda)) {
                                texto = parseoNumeros.get(busqueda) + " " + texto;
                            } else {
                                texto = parseoNumeros.get(valor) + "cientos " + texto;
                            }
                        }

                    }
                }
            }
        }
        return texto;
    }

    //Constructor para buscar el numero en decenas
    public static String decenas(int busqueda, String resultado, int NumeroUno, int NumeroDos, int valor, int unidad) {
        if (parseoNumeros.containsKey(busqueda)) {
            resultado = parseoNumeros.get(busqueda);
        } else {
            if (busqueda > 20 && busqueda < 30) {
                resultado = "veinti" + parseoNumeros.get(NumeroUno);
            } else {
                if (busqueda > 15 && busqueda < 20) {
                    resultado = "dieci" + parseoNumeros.get(NumeroUno);
                } else {
                    busqueda = NumeroDos * 10;
                    if ((unidad == 1 || unidad == 2) && NumeroUno == 1) {
                        if (parseoNumeros.containsKey(busqueda)) {
                            resultado = parseoNumeros.get(busqueda);
                            resultado = resultado + " y un";
                        }
                    } else {
                        if (parseoNumeros.containsKey(busqueda)) {
                            resultado = parseoNumeros.get(busqueda);
                            resultado = resultado + " y " + parseoNumeros.get(NumeroUno);
                        }
                    }

                }
            }
        }
        return resultado;
    }

    private static void crearParseNumeros() {
        parseoNumeros.put(0,"cero");
        parseoNumeros.put(1,"uno");
        parseoNumeros.put(2,"dos");
        parseoNumeros.put(3,"tres");
        parseoNumeros.put(4,"cuatro");
        parseoNumeros.put(5,"cinco");
        parseoNumeros.put(6,"seis");
        parseoNumeros.put(7,"siete");
        parseoNumeros.put(8,"ocho");
        parseoNumeros.put(9,"nueve");
        parseoNumeros.put(10, "diez");
        parseoNumeros.put(11, "once");
        parseoNumeros.put(12, "doce");
        parseoNumeros.put(13, "trece");
        parseoNumeros.put(14, "catorce");
        parseoNumeros.put(15, "quince");
        parseoNumeros.put(20, "veinte");
        parseoNumeros.put(30, "treinta");
        parseoNumeros.put(40, "cuarenta");
        parseoNumeros.put(50, "cincuenta");
        parseoNumeros.put(60, "sesenta");
        parseoNumeros.put(70, "setenta");
        parseoNumeros.put(80, "ochenta");
        parseoNumeros.put(90, "noventa");
        parseoNumeros.put(100, "cien");
        parseoNumeros.put(500, "quinientos");
        parseoNumeros.put(1000, "mil");
        parseoNumeros.put(1000000, "un millon");
    }
}
